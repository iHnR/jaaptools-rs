pub trait JaapResult {
    fn for_error<F: FnOnce() -> ()>(self, f: F) -> Self;
}

impl<T, E> JaapResult for Result<T, E> {
    fn for_error<F: FnOnce() -> ()>(self, f: F) -> Self {
        if self.is_err() {
            f();
        }
        self
    }
}
