use core::iter::{Cycle, Peekable, Skip, Take};
use core::ops::{AddAssign, Sub};
use itertools::{Intersperse, Itertools};
use num::Num;
use std::time::Instant;

impl<T: Iterator + Itertools + Sized> IterJaap for T {}

pub trait IterJaap: Iterator + Itertools {
    /// Rotates the iterator to the left by n
    /// Only available for iterators with an exact size
    /// Sample:
    /// ```
    /// use jaaptools::iter::IterJaap;
    /// let iter = (0..5); // Your iterator here
    /// assert_eq!(iter.rotate_left(2).collect::<Vec<_>>(), [2,3,4,0,1]);
    /// ```
    /// The method above is also how this is implemented, where the RotateLeft struct is just a
    /// wrapper arount a `Take<Skip<Cycle<impl Iterator>>>`
    ///
    /// Note that this method could have unintended consequences if the encapsulated iterator has
    /// any side effects.
    fn rotate_left(self, n: usize) -> Rotate<Self>
    where
        Self: Clone + ExactSizeIterator,
    {
        let length = self.len();
        let n = n % length;
        self.cycle().skip(n).take(length)
    }

    /// Rotates the iterator to the right by n
    /// Only available for iterators with an exact size
    /// Sample:
    /// ```
    /// use jaaptools::iter::IterJaap;
    /// let iter = (0..5); // Your iterator here
    /// assert_eq!(iter.rotate_right(2).collect::<Vec<_>>(), [3,4,0,1,2]);
    /// ```
    /// The method above is also how this is implemented, where the `RotateRight` struct is just a
    /// wrapper arount a `Take<Skip<Cycle<impl Iterator>>>`
    ///
    /// Note that this method could have unintended consequences if the encapsulated iterator has
    /// any side effects.
    fn rotate_right(self, n: usize) -> Rotate<Self>
    where
        Self: Clone + ExactSizeIterator,
    {
        let length = self.len();
        let n = n % length; // Prevent unneccessary cycling
        self.cycle().skip(length - n).take(length)
    }

    /// Iterate over the accumulative sum of an iterator
    /// Example:
    /// ```
    /// use jaaptools::iter::IterJaap;
    /// let slice = [1, 2, 3, 4, 5];
    /// assert_eq!(
    ///     slice.into_iter().cumsum().collect::<Vec<_>>(),
    ///     [1, 3, 6, 10, 15]
    /// );
    /// ```
    /// This is analogous to the numpy `.cumsum()` method
    /// ```python
    /// import numpy as np
    /// a = np.array([1,2,3,4,5])
    /// assert a.cumsum() == np.array([1,3,6,10,15])
    /// ```
    fn cumsum(self) -> CumSum<Self>
    where
        Self: Sized,
        Self::Item: Default + AddAssign<Self::Item> + Copy,
    {
        CumSum {
            iter: self,
            acc: Self::Item::default(),
        }
    }

    /// Allows to peek back at the previous value
    /// This could also be considered the current value, depending on your perspective.
    /// ```
    /// use jaaptools::iter::IterJaap;
    /// let mut iterator = (3..10).backpeekable();
    /// assert_eq!(iterator.peek_back(), None);
    /// let _ = iterator.next();
    /// assert_eq!(iterator.peek_back(), Some(3));
    /// ```
    fn backpeekable(self) -> BackPeekable<Self>
    where
        Self: Sized,
    {
        BackPeekable {
            iter: self,
            previous: None,
        }
    }

    /// Intersperses `value` every `size` items
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let v1:Vec<_> = (0..7).intersperse_runs(2,-1).collect();
    /// assert_eq!(vec![0,1,-1,2,3,-1,4,5,-1,6], v1);
    /// ```
    fn intersperse_runs(self, size: usize, value: <Self as Iterator>::Item) -> IntersperseRuns<Self>
    where
        Self: Sized,
        <Self as Iterator>::Item: Clone,
    {
        IntersperseRuns {
            iter: self,
            size,
            index: 0,
            value,
            done_this: true,
        }
    }

    /// Does the same as `intersperse` but also adds the element to the start and end
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let v:Vec<_> = (1..=3).outersperse(0).collect();
    /// assert_eq!(v , vec![0,1,0,2,0,3,0]);
    /// ```
    fn outersperse(self, value: <Self as Iterator>::Item) -> Outersperse<Self>
    where
        Self: Sized,
        <Self as Iterator>::Item: Clone,
    {
        #[allow(unstable_name_collisions)]
        self.intersperse(value.clone()).surround(value)
    }

    /// Surround the iterator with a value at the start and end
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let v:Vec<_> = (0..3).surround(42).collect();
    /// assert_eq!(v , vec![42,0,1,2,42]);
    /// ```
    fn surround(self, value: <Self as Iterator>::Item) -> Surround<Self>
    where
        Self: Sized,
        <Self as Iterator>::Item: Clone,
    {
        self.prepend(value.clone()).append(value)
    }

    /// Prepends a single value to the iterator
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let v:Vec<_> = ["world","!"].into_iter().prepend("hello").collect();
    /// assert_eq!(v , vec!["hello","world","!"]);
    /// ```
    fn prepend(self, value: <Self as Iterator>::Item) -> Prepend<Self>
    where
        Self: Sized,
    {
        [value].into_iter().chain(self)
    }

    /// Appends a single value to the iterator
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let v:Vec<_> = ["hello","world"].into_iter().append("!").collect();
    /// assert_eq!(v , vec!["hello","world","!"]);
    /// ```
    fn append(self, value: <Self as Iterator>::Item) -> Append<Self>
    where
        Self: Sized,
    {
        self.chain([value].into_iter())
    }

    /// Returns an iterator that yield the time at which `next` was called
    /// This could be useful when doing a bunch of computations that can take a long time and you
    /// want to know at what time each was completed.
    fn timer() -> TimeIter {
        TimeIter
    }

    /// Iterate over consecutive pairs
    /// skips the last value if length is odd
    fn pairs(self) -> Pairs<Self>
    where
        Self: Sized,
    {
        Pairs { iter: self }
    }

    fn recursive_iterator<T, F>(init: T, f: F) -> RecursiveIterator<T, F>
    where
        T: Clone,
        F: Fn(&T) -> T,
    {
        RecursiveIterator {
            state: init,
            callback: f,
        }
    }

    /// Find the intersection between two sorted iterators without repeated values
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let iter1 = [2, 3, 5].into_iter();
    /// let iter2 = [2, 5, 6].into_iter();
    /// let intersection: Vec<_> = iter1.intersect(iter2).collect();
    /// assert_eq!(intersection, [2, 5]);
    /// ```
    fn intersect<I>(self, iter2: I) -> Intersection<Self, I>
    where
        I: Iterator,
        Self: Sized,
    {
        Intersection { iter1: self, iter2 }
    }

    /// Find the intersection between two sorted iterators without repeated values
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let iter = [1, 2, 3, 5, 6, 8, 10,11].into_iter();
    /// let result: Vec<_> = iter.collapse_consecutives().collect();
    /// assert_eq!(result, [3,6,8,11]);
    /// ```
    fn collapse_consecutives(self) -> CollapseConsecutives<Self>
    where
        Self: Sized,
    {
        CollapseConsecutives {
            iter: self.peekable(),
        }
    }

    /// Iterate over the difference between consecutive elements
    /// A zero element is implicity prepended to the iterator
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let iter = [3,4,7,5,8].into_iter();
    /// let result = iter.difference().collect::<Vec<_>>();
    /// assert_eq!(result, [3,1,3,-2,3]);
    ///
    /// let iter = [0,4,5].into_iter();
    /// let result = iter.difference().collect::<Vec<_>>();
    /// assert_eq!(result, [0,4,1]);
    fn difference(self) -> Difference<Self>
    where
        Self: Sized,
        Self::Item: Copy + Sub,
    {
        Difference {
            iter: self.backpeekable(),
        }
    }

    /// Return an iterator that returns tuples of consecutive elements
    /// ```
    /// use jaaptools::iter::IterJaap;
    ///
    /// let result:Vec<_> = (1..=4).advance_tuple().collect();
    /// assert_eq!(result, [(1,2),(2,3),(3,4)]);
    /// ```
    fn advance_tuple(self) -> AdvanceTuple<Self>
    where
        Self: Sized,
        Self::Item: Copy,
    {
        AdvanceTuple {
            iter: self,
            previous: None,
        }
    }
}

pub type Rotate<T> = Take<Skip<Cycle<T>>>;
pub type Append<T> = std::iter::Chain<T, std::array::IntoIter<<T as Iterator>::Item, 1>>;
pub type Prepend<T> = std::iter::Chain<std::array::IntoIter<<T as Iterator>::Item, 1>, T>;
/// Type alias for the return of surround
pub type Surround<T> = Append<Prepend<T>>;
/// Type alias for the return of outersperse
pub type Outersperse<T> = Surround<Intersperse<T>>;

pub struct TimeIter;

impl Iterator for TimeIter {
    type Item = Instant;
    fn next(&mut self) -> Option<Self::Item> {
        Some(Instant::now())
    }
}

pub struct IntersperseRuns<I>
where
    I: Iterator,
    <I as Iterator>::Item: Clone,
{
    iter: I,
    size: usize,
    index: usize,
    value: <I as Iterator>::Item,
    done_this: bool,
}

impl<I> Iterator for IntersperseRuns<I>
where
    I: Iterator,
    <I as Iterator>::Item: Clone,
{
    type Item = <I as Iterator>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index % self.size == 0 && !self.done_this {
            self.done_this = true;
            Some(self.value.clone())
        } else {
            self.done_this = false;
            self.index += 1;
            self.iter.next()
        }
    }
}

pub struct BackPeekable<I>
where
    I: Iterator,
{
    iter: I,
    previous: Option<<I as Iterator>::Item>,
}

impl<I> Iterator for BackPeekable<I>
where
    I: Iterator,
    <I as Iterator>::Item: Copy,
{
    type Item = <I as Iterator>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.previous = self.iter.next();
        self.previous
    }
}

impl<I> BackPeekable<I>
where
    I: Iterator,
    <I as Iterator>::Item: Copy,
{
    pub fn peek_back(&self) -> Option<<Self as Iterator>::Item> {
        self.previous
    }
}

#[derive(Debug)]
pub struct CumSum<I>
where
    I: Iterator,
    I::Item: Default + AddAssign<I::Item> + Copy,
{
    iter: I,
    acc: I::Item,
}

impl<I> Iterator for CumSum<I>
where
    I: Iterator,
    I::Item: Default + AddAssign<I::Item> + Copy,
{
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.acc += self.iter.next()?;
        Some(self.acc)
    }
}

#[derive(Debug)]
pub struct Pairs<I: Iterator> {
    iter: I,
}

impl<I> Iterator for Pairs<I>
where
    I: Iterator,
{
    type Item = (I::Item, I::Item);

    fn next(&mut self) -> Option<Self::Item> {
        let first = self.iter.next()?;
        let second = self.iter.next()?;
        Some((first, second))
    }
}

#[derive(Debug)]
pub struct RecursiveIterator<T, F>
where
    F: Fn(&T) -> T,
    T: Clone,
{
    state: T,
    callback: F,
}

impl<T, F> Iterator for RecursiveIterator<T, F>
where
    F: Fn(&T) -> T,
    T: Clone,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        let next_state = (self.callback)(&self.state);
        self.state = next_state.clone();
        Some(next_state)
    }
}

pub struct CollapseConsecutives<I>
where
    I: Iterator,
{
    iter: Peekable<I>,
}

impl<I> Iterator for CollapseConsecutives<I>
where
    I: Iterator<Item = usize>,
{
    type Item = I::Item;
    fn next(&mut self) -> Option<Self::Item> {
        let next_value = self.iter.next()?;
        match self.iter.peek() {
            Some(&value) if value == next_value + 1 => self.next(),
            _ => Some(next_value),
        }
    }
}

pub struct Intersection<I, J>
where
    I: Iterator,
    J: Iterator,
{
    iter1: I,
    iter2: J,
}

impl<I, J> Intersection<I, J>
where
    I: Iterator,
    J: Iterator,
    I::Item: Eq,
    J::Item: Eq,
{
    pub fn new(iter1: I, iter2: J) -> Self {
        Self { iter1, iter2 }
    }
}

impl<I, J, T> Iterator for Intersection<I, J>
where
    I: Iterator<Item = T>,
    J: Iterator<Item = T>,
    T: Ord,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        let mut v1 = self.iter1.next()?;
        let mut v2 = self.iter2.next()?;
        loop {
            if v1 < v2 {
                v1 = self.iter1.next()?;
            } else if v1 > v2 {
                v2 = self.iter2.next()?;
            } else {
                break Some(v1);
            }
        }
    }
}

pub struct Difference<I>
where
    I: Iterator,
    I::Item: Sub + Copy,
{
    iter: BackPeekable<I>,
}

impl<I> Iterator for Difference<I>
where
    I: Iterator,
    I::Item: Sub<Output = I::Item> + Copy + Default,
{
    type Item = I::Item;
    fn next(&mut self) -> Option<Self::Item> {
        let last = self.iter.peek_back().unwrap_or(Default::default());
        self.iter.next().map(|next| next - last)
    }
}

pub struct AdvanceTuple<I>
where
    I: Iterator,
    I::Item: Copy,
{
    iter: I,
    previous: Option<I::Item>,
}

impl<I> Iterator for AdvanceTuple<I>
where
    I: Iterator,
    I::Item: Copy,
{
    type Item = (I::Item, I::Item);
    fn next(&mut self) -> Option<Self::Item> {
        match (self.previous, self.iter.next()) {
            (None, Some(value)) => {
                self.previous = Some(value);
                self.next()
            }
            (Some(previous), Some(next)) => {
                self.previous = Some(next);
                Some((previous, next))
            }
            _ => None,
        }
    }
}

pub trait SumResult<T: Num, E>: Iterator<Item = Result<T, E>> {
    /// Sums the values of an Iterator over Result Values, shortcutting if it finds an Err
    /// This allows summing with constant stack space and no heap space, rather than the
    /// default way of first collecting into a Vec and then summing.
    fn sum_result(self) -> Result<T, E>
    where
        Self: Sized,
    {
        let mut acc = T::zero();
        for item in self {
            acc = acc + item?;
        }
        Ok(acc)
    }
}

impl<T: Num, E, I: Iterator<Item = Result<T, E>>> SumResult<T, E> for I {}
